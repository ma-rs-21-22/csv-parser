# encoding="utf8",
# encoding='iso-8859-1'

import statistics
import pandas as pd
import numpy as np
import csv
import json
import io
import dtale
from pandas_profiling import ProfileReport

############################
####### Demo Data ##########
############################

# Datensätze von Data.Europa.eu

example_1 = 'de-nrw-dortmund-witterungsverhaeltnisse_seit_1990.csv'
example_2 = 'addresses.csv'
example_3 = 'de-nrw-dortmund-strassenverkehrsunfaelle_mit_personen-_und_schwerwiegendem_sachschaden_nach_ortslagen_seit_1990.csv'
example_4 = 'de-nrw-dortmund-zu-_und_abgang_von_arbeitslosen_seit_1990.csv'
example_5 = 'de-nrw-dortmund-versorgung_mit_energie_und_wasser_seit_1994.csv'
example_7 = 'de-nrw-dortmund-steueraufkommen_nach_ausgewaehlten_steuerarten_bei_den_dortmunder_finanzaemtern_seit_1990.csv'
example_8 = 'de-nrw-dortmund-realschulen_schulabgaengerinnen_nach_abschluss_seit_1990.csv'
example_9 = 'de-nrw-dortmund-straftaten_nach_art_seit_1990.csv'
example_11 = 'altersstruktur_statistischeRaumeinheiten_31122019_0.csv'
example_12 = 'wahllokale-20210926.csv'

dublin_analyse = '2021-dublin-city-cycle-counts-08122021.csv'

# Analysierter Datensatz:
demo_dataset = example_9


# path
path = '/Users/stb/Documents/Privat/__Universität/MASTERARBEIT/05_Prototype/csv_parser_service/example_data/'


##############################
###### 1 CSV import ##########
##############################

# Determine the delimeter
with io.open(path+demo_dataset, 'r', encoding='iso-8859-1') as csvfile:
    dialect = csv.Sniffer().sniff(csvfile.readline())
    print('######## The Delimiter is ' + dialect.delimiter)
    sep = dialect.delimiter

# Create Dataframe
df = pd.read_csv(path+demo_dataset, sep=sep)


####################################
#### 2 PANDAS Profiling Report #####
####################################

# Run Report Calculation 
# df.head(100) --> nur die ersten 1000 zeilen
profile = ProfileReport(df.head(1000), title="Pandas Profiling Report", explorative=True)

# Create Report Files:
profile.to_file("your_report.html")
profile.to_file("your_report.json")


###############################
###### Eigene Analyse #########
###############################

# 1 Meaning + Type analyse
# numeric_only ist auf True, weil strings in "" einen Error erzeugen. Außerdem müssen strings nicht numerisch metrisch analysiert werden.

print(df)
print(df.mean(axis=0, numeric_only=True))
print(df.dtypes)
print(df.columns)


# Convert Dataframe (csv) to json um dann die analyseinformationen in das Json hinzuzufügen
# result = df.to_json(orient="table")  # orient="split" > auch interessant


# Save as JSON

# parsed = json.loads(result)
# json.dumps(parsed, indent=4)
# print(json.dumps(parsed, indent=4))

